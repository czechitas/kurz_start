---
title:  8. lekce - Hosting, domény, Internet
date:   2017-03-08
---

### Jak funguje Internet

- služby Internetu: [web](https://cs.wikipedia.org/wiki/World_Wide_Web), [e-mail](https://cs.wikipedia.org/wiki/E-mail), [torrent](https://cs.wikipedia.org/wiki/BitTorrent), [FTP](https://cs.wikipedia.org/wiki/File_Transfer_Protocol), ...
- web funguje na protokolu HTTP(S) a stáhnutí webové stránky probíhá cca takto:
  - Nešifrovaná stránka s obrázkem z FB:

    ![Diagram HTTP](https://www.planttext.com/plantuml/svg/bP9BReCm48RtFiLS84PfysXPj5barKfLf2wwDZ091iCWsqH9nHLwX-O0kK8tDFSgi414HAgyiMJzZq_ZySfOeMsPASQij0eXjhRmFGyF8YiKyf0oZx6YySXG5YAti7vxlxflDCMgFbz-yFVhIlZ0mA3UesPiw879KzlXmsk4a20CIC-i4LfzwFVX1I4NCBsxvpC-vuibgwfgLDdJV5XZ0fa4GsL06ZpMI-Fg3PdwhEfJrhjb7k61nxPv4JUCiKU5XeV6k047EJVWbcmgU5Bit7XjxDZ5Rk66czcA40EYzD_LB9RTPgOFZrtdJbDPmELRRZNjwrDE3aDHeCKdffDcsxEv-t3ZEEOHOwlcRF_u7m00)

  - Šifrovaná stránka:

    ![Diagram HTTPS](https://www.planttext.com/plantuml/svg/fP9DJiCm48NtFiLSe8uA_O4ie8kkG4ALMB1saadpusGiswbQb2jm1tg0Ne5DwBs8kwXIfH94R2nPxpr_6hzP62kqhGh9c4sjH4YiLSRtFDoAGada8HKUOwerfM6gH6bX-V9wSLzfIcHpE7xXzyU5y8Q1GRr1pLZN0wC7zuOFpn52Xj0XFR9MS7hNxyCJGYbWV7FB9tpAPtDMrpLRQweK73_JMDEcsPVKWvpvFYmnWo85GrL06Znsblgm5Pdc89jzSNQdPOHRdjZsanpuxnXZTn8D3uqP1kpaXW5ZihdWcLeFuxdOxXJ3ChgAxFOLYn03elpNmcRpKr_Zk_j_5qQ15k-OZzeTc4o73zYFkq7PeZtTwly0)

  - Otevření dálší stránky, kdy se už na většinu zdrojů využívá cache:

    ![Diagram cache](https://www.planttext.com/plantuml/svg/ZP8nJiD044NxFSLSmBQ0qBY0IAIE8b6aNZlZUEDTpsfd707o5RW3EK2kG6DoB-mKKQmWv7Aq_-t_VpHp5XsaSLOfCM8HIX7FQPBWctRUOfoJIpRQiebOWguh7PSog7sFcTnuNGiibg-ZUHMejDtnz8q_dwE7DMQCOOz1gMiDH4yZ9eKNdPS8LncULTksQXkeyN3wCaMWVNUewUQRYqCA2zo1Cy3KP1JWhn8NyMs05N5tjDt1_STZlJd7cj8bONcto77EF9qfI7gldTzEHWgTOqPK3JkRxaDPq1zOHNVt3xF7iv-Qz-M6i_W5)


### Co je potřeba pro zveřejnění svého webu

- Hosting
  - pronájem prostoru na webserveru
  - základní hosting bývá často zdarma k doméně (Wedos)
  - soubory webu se na hosting nahrávají nejčastěji přes FTP nebo GIT
- Doména
  - aby DNS překládalo "example.com" na IP adresu
  - je samostatná služba, ale většinou poskytují hostingové firmy
  - stojí pár set korun, subdomény (neco.example.com) nabízejí hostingy obvykle zdarma
